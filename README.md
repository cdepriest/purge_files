# Purge_Files

### Purpose 
To illistratre that purging of files in git will still make them accessable to the community even if they do not appear in the git history.

### Step to reproduce

* Add File to be deleted (DELETE_ME.MD)
* Add another file to keep (KEEP_ME.MD)
* Purge localy 
* Force push 
 


### Steps of purge
This is using a single branch if multiple branches each branch needs to be addressed.
In this example `path/to/file` is replaced with `DELET_ME.MD`
```bash
git filter-branch --force --index-filter \
'git rm --cached --ignore-unmatch path/to/bad/file' \
--prune-empty --tag-name-filter cat -- --all
```

Once complete nuke history

```bash
rm -rf .git/refs/original/
git reflog expire --expire=now --all
git gc --aggressive --prune=now
```
Push back origin ensure you remove protected branch if needed when force pushing

```bash
git push -f origin --all
# push any tags as well
git push -f origin --tags
```

Running the above commands removes the commit for adding the delete me HOWEVER if I go to my Activity it referances a commit that was removed 
[here](https://gitlab.com/cdepriest/purge_files/commit/c99c9e65c5562ccf1ddc161a029c5079c4433df3)

Which the users profile is public this is bad

you can see the commit was removed from history 

This shows the first commit

` git show e68a65da` 

And tryignt to retrieve the commit listed in my activity shows nothing

`git show c99c9e65`


However if I go to my profile 
[here](https://gitlab.com/cdepriest)

You can see the activity of the deleted commit of "c99c9e65 · Add DELETE_ME"
EHich takes you to the removed file with my secrets.






# Using BFG
I was instructed to use BFG and it will fix this issue

```bash
git clone git@gitlab.com:cdepriest/purge_files.git
sudo yum install git
git clone git@gitlab.com:cdepriest/purge_files.git
sudo yum install java-1.8.0-openjdk
java -jar bfg-1.13.0.jar --delete-files DELETE_ME2.MD purge_files
cd purge_files
git reflog expire --expire=now --all && git gc --prune=now --aggressive
git push -f

```


So after following these instructions **SAME PROBLEM**

You can see the commit history for commits that DO NOT exists but is linkable via Activity
https://gitlab.com/cdepriest/purge_files/commit/00ffe812aefc192952470f428ccae806cdd23407


## How to prevent being seen
```bash
Log into Gitaly Server 
su - git$ cd /path/to/repo/project.git$
git reflog expire --expire=now --all && git gc --prune=now --aggressive


```
